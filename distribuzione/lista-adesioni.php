<?php
include('template/include/header.php');
include('template/include/siedenav.php');
?>

    <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

        <?php
        $breadcrumb = 'Lista adesioni';
        include('template/include/topnav.php');
        ?>

        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div class="container" id="kt_content_container">



                <?php
                include('template/moduli/dialogatore/header-profilo.php');
                ?>





                    <?php
                    include('template/moduli/wdg-table-lista-adesioni.php');
                    ?>




            </div>
        </div>

        <?php
        include('template/include/footer-content.php');
        ?>
    </div>

<?php
include('template/parziali/_panel_activiti.php');
include('template/parziali/_panel_chat.php');
//include('template/parziali/_panel_explore.php');
include('template/include/footer.php');
?>

