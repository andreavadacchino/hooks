<!DOCTYPE html>
<html lang="it">
<!--begin::Head-->
<head><base href="">
    <meta charset="utf-8" />
    <title>Hooks</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link rel="canonical" href="" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="" />
    <meta property="og:url" content="" />
    <meta property="og:site_name" content="Hooks" />

    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="assets/media/logos/favicon.ico" />
    <!--begin::Fonts-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700" />
    <!--end::Fonts-->

    <!--begin::Global Stylesheets Bundle(used by all pages)-->
    <link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
    <link href="assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Stylesheets Bundle-->
</head>



<body id="kt_body" class="bg-body">
<!--begin::Main-->
<div class="d-flex flex-column flex-root">
    <!--begin::Authentication - Signup Free Trial-->
    <div class="d-flex flex-column flex-xl-row flex-column-fluid">
        <!--begin::Aside-->
        <div class="d-flex flex-column flex-lg-row-fluid">
            <!--begin::Wrapper-->
            <div class="d-flex flex-row-fluid flex-center p-10">
                <!--begin::Content-->
                <div class="d-flex flex-column">
                    <!--begin::Logo-->
                    <a href="../../demo3/dist/index.html" class="mb-2">
                        <img alt="Logo" src="assets/media/logos/logo-hooks.png" class="h-120px"/>

                    </a>
                    <!--end::Logo-->
                    <!--begin::Title-->
                    <h1 class="text-dark fs-2x mb-3">Benvenuto!</h1>
                    <!--end::Title-->
                    <!--begin::Description-->
                    <div class="fw-bold fs-4 text-gray-400 mb-10">Lorem Ipsum is simply dummy text of the printing
                        <br>and typesetting industry.</div>
                    <!--begin::Description-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Wrapper-->
            <!--begin::Illustration-->
            <div class="d-flex flex-row-auto bgi-no-repeat bgi-position-x-center bgi-size-contain bgi-position-y-bottom min-h-200px min-h-xl-300px mb-xl-10" style="background-image: url(assets/media/illustrations/networks.png)"></div>
            <!--end::Illustration-->
        </div>
        <!--begin::Aside-->
        <!--begin::Content-->
        <div class="flex-row-fluid d-flex flex-center justfiy-content-xl-first p-10">
            <!--begin::Wrapper-->
            <div class="d-flex flex-center p-15 shadow rounded w-100 w-md-550px mx-auto ms-xl-20">


                <form class="form w-100" novalidate="novalidate" id="kt_password_reset_form">
                    <!--begin::Heading-->
                    <div class="text-center mb-10">
                        <!--begin::Title-->
                        <h1 class="text-dark mb-3">Ha dimenticato la password?</h1>
                        <!--end::Title-->
                        <!--begin::Link-->
                        <div class="text-gray-400 fw-bold fs-4">Inserisci la tua email per reimpostare la password.</div>
                        <!--end::Link-->
                    </div>
                    <!--begin::Heading-->
                    <!--begin::Input group-->
                    <div class="fv-row mb-10">
                        <label class="form-label fw-bolder text-gray-900 fs-6">Email</label>
                        <input class="form-control form-control-solid" type="email" placeholder="" name="email" autocomplete="off" />
                    </div>
                    <!--end::Input group-->
                    <!--begin::Actions-->
                    <div class="d-flex flex-wrap justify-content-center pb-lg-0">
                        <button type="button" id="kt_password_reset_submit" class="btn btn-lg btn-primary fw-bolder me-4">
                            <span class="indicator-label">Invia</span>
                            <span class="indicator-progress">Please wait...
									<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                        <a href="/login" class="btn btn-lg btn-light-primary fw-bolder">Annulla</a>
                    </div>
                    <!--end::Actions-->
                </form>



            </div>
            <!--end::Wrapper-->
        </div>
        <!--end::Right Content-->
    </div>
    <!--end::Authentication - Signup Free Trial-->
</div>


<script src="assets/plugins/global/plugins.bundle.js"></script>
<script src="assets/js/scripts.bundle.js"></script>

<script src="assets/js/hooks/authentication/password-reset/password-reset.js"></script>
</body>
<!--end::Body-->
</html>