<?php
include('template/include/header.php');
include('template/include/siedenav.php');
?>
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
    <?php
    $breadcrumb = 'Badge';
    include('template/include/topnav.php');
    ?>
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="container" id="kt_content_container">
            <?php
            include('template/moduli/dialogatore/header-profilo.php');
            ?>
            <div class="row g-6 g-xl-9 mb-5">
                <div class="col-xl-7 mb-5">
                    <?php
                    include('template/moduli/wdg-badge-progress.php');
                    ?>
                </div>
                <div class="col-xl-5 mb-5">
                    <?php
                    include('template/moduli/wdg-badge-pending.php');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    include('template/include/footer-content.php');
    ?>
</div>
<?php
include('template/include/footer.php');
?>

