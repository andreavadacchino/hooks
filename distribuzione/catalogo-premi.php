<?php
include('template/include/header.php');
include('template/include/siedenav.php');
?>

    <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">

        <?php
        $breadcrumb = 'Catalogo premi';
        include('template/include/topnav.php');
        ?>

        <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
            <div class="container" id="kt_content_container">


                <div class="d-flex flex-column flex-xl-row">

                    <div class="flex-column flex-lg-row-auto w-100 w-xl-300px mb-10" id="kt_profile_aside">

                        <div class="card card-custom gutter-b">

                            <div class="card-body">

                                <form>
                                    <!--begin::Categories-->
                                    <div class="form-group mb-11">
                                        <label class="font-size-h3 font-weight-bolder text-dark mb-7">Categories</label>

                                        <div class="checkbox-list">
                                            <label class="checkbox checkbox-lg mb-7">
                                                <input type="checkbox" name="electronics" />
                                                <span></span>
                                                <div class="font-size-lg text-dark-75 font-weight-bold">Electronics</div>
                                                <div class="ml-auto text-muted font-weight-bold">28</div>
                                            </label>
                                            <label class="checkbox checkbox-lg mb-7">
                                                <input type="checkbox" name="sportsequipment" />
                                                <span></span>
                                                <div class="font-size-lg text-dark-75 font-weight-bold">Sports Equipments</div>
                                                <div class="ml-auto text-muted font-weight-bold">307</div>
                                            </label>
                                            <label class="checkbox checkbox-lg mb-7">
                                                <input type="checkbox" name="appliances" />
                                                <span></span>
                                                <div class="font-size-lg text-dark-75 font-weight-bold">Appliances</div>
                                                <div class="ml-auto text-muted font-weight-bold">54</div>
                                            </label>
                                            <label class="checkbox checkbox-lg mb-7">
                                                <input type="checkbox" name="appliances" />
                                                <span></span>
                                                <div class="font-size-lg text-dark-75 font-weight-bold">Software Solutions</div>
                                                <div class="ml-auto text-muted font-weight-bold">762</div>
                                            </label>
                                            <label class="checkbox checkbox-lg">
                                                <input type="checkbox" name="appliances" />
                                                <span></span>
                                                <div class="font-size-lg text-dark-75 font-weight-bold">Food &amp; Groceries</div>
                                                <div class="ml-auto text-muted font-weight-bold">95</div>
                                            </label>
                                        </div>
                                        
                                    </div>

                                    <button type="reset" class="btn btn-clear font-weight-bolder text-muted px-8">Filtra</button>
                                </form>

                            </div>

                        </div>

                    </div>




                    <div class="flex-lg-row-fluid ms-lg-15">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch gutter-b">
                            <div class="card-body">
                                <!--begin::Section-->
                                <div class="card card-custom">
                                    <div class="card-body p-0">
                                        <div class="row">



                                            <div class="col-md-6 col-lg-12 col-xxl-6">
                                                <!--begin::Featured Category-->
                                                <div class="card card-custom card-stretch card-stretch-half gutter-b mb-10">
                                                    <div class="card-body p-0 d-flex rounded" style="background:radial-gradient(94.09% 94.09% at 50% 50%, rgba(255, 255, 255, 0.45) 0%, rgba(255, 255, 255, 0) 100%), #FFA800;">
                                                        <div class="row m-0">
                                                            <div class="col-7 p-0">
                                                                <div class="card card-custom card-stretch bg-transparent shadow-none">
                                                                    <div class="card-body d-flex flex-column justify-content-center pr-0">
                                                                        <h3 class="font-size-h4 font-size-h1-sm font-size-h4-lg font-size-h1-xl mb-0">
                                                                            <a href="#" class="text-white font-weight-bolder">Smart Watches</a>
                                                                        </h3>
                                                                        <div class="font-size-lg font-size-h4-sm font-size-h6-lg font-size-h4-xl text-dark">Make It Amazing</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="d-flex flex-center col-5 p-0">
                                                                <img src="assets/media/products/1.png" class="d-flex flex-row-fluid w-100" style="transform: scale(1.3);" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Featured Category-->
                                                <!--begin::Featured Category-->
                                                <div class="card card-custom card-stretch card-stretch-half gutter-b">
                                                    <div class="card-body p-0 d-flex rounded" style="background: radial-gradient(110.23% 110.23% at 50% 50%, rgba(255, 255, 255, 0.27) 0%, rgba(255, 255, 255, 0) 100%), #8950FC;">
                                                        <div class="row m-0">
                                                            <div class="col-7 p-0">
                                                                <div class="card card-custom card-stretch bg-transparent shadow-none">
                                                                    <div class="card-body d-flex flex-column justify-content-center pr-0">
                                                                        <h3 class="font-size-h4 font-size-h1-sm font-size-h4-lg font-size-h1-xl mb-0">
                                                                            <a href="#" class="text-white font-weight-bolder">Headphones</a>
                                                                        </h3>
                                                                        <div class="font-size-lg font-size-h4-sm font-size-h6-lg font-size-h4-xl text-dark">Make It Amazing</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-5 p-0 d-flex flex-center">
                                                                <img src="assets/media/products/21.png" class="d-flex flex-row-fluid w-100" style="transform: scale(1.3);" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Featured Category-->
                                            </div>



                                            <div class="col-md-6 col-lg-12 col-xxl-6">
                                                <!--begin::Featured Category-->
                                                <div class="card card-custom card-stretch gutter-b">
                                                    <div class="card-body d-flex flex-column rounded justify-content-between p-14" style="background: radial-gradient(83.15% 83.15% at 50% 50%, rgba(255, 255, 255, 0.35) 0%, rgba(255, 255, 255, 0) 100%), #1BC5BD;">
                                                        <img src="assets/media/products/22.png" class="w-100" style="transform: scale(1.2);" />
                                                        <div class="text-center">
                                                            <h3 class="font-size-h1 mb-0">
                                                                <a href="#" class="text-white font-weight-bolder">Smart Toys</a>
                                                            </h3>
                                                            <div class="font-size-h4 text-dark">Get Amazing Toys</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end::Featured Category-->
                                            </div>


                                        </div>
                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-4 col-lg-12 col-xxl-4">
                                        <div class="card card-custom gutter-b card-stretch">
                                            <div class="card-body d-flex flex-column rounded bg-light justify-content-between">
                                                <div class="text-center rounded mb-7">
                                                    <img src="assets/media/products/15.png" class="mw-100 w-200px" />
                                                </div>
                                                <div>
                                                    <h4 class="font-size-h5">
                                                        <a href="#" class="text-dark-75 font-weight-bolder">Bose Headphones 700</a>
                                                    </h4>
                                                    <div class="font-size-h6 text-muted font-weight-bolder">$370.00</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-12 col-xxl-4">
                                        <div class="card card-custom gutter-b card-stretch">
                                            <div class="card-body d-flex flex-column rounded bg-light justify-content-between">
                                                <div class="text-center rounded mb-7">
                                                    <img src="assets/media/products/16.png" class="mw-100 w-200px" />
                                                </div>
                                                <div>
                                                    <h4 class="font-size-h5">
                                                        <a href="#" class="text-dark-75 font-weight-bolder">Amazon Bip Home</a>
                                                    </h4>
                                                    <div class="font-size-h6 text-muted font-weight-bolder">$370.00</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-lg-12 col-xxl-4">
                                        <div class="card card-custom gutter-b card-stretch">
                                            <div class="card-body d-flex flex-column rounded bg-light justify-content-between">
                                                <div class="text-center rounded mb-7">
                                                    <img src="assets/media/products/17.png" class="mw-100 w-200px" />
                                                </div>
                                                <div>
                                                    <h4 class="font-size-h5">
                                                        <a href="#" class="text-dark-75 font-weight-bolder">Black &amp; White Vases</a>
                                                    </h4>
                                                    <div class="font-size-h6 text-muted font-weight-bolder">$370.00</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>

                    </div>

                </div>


            </div>
        </div>

        <?php
        include('template/include/footer-content.php');
        ?>
    </div>

<?php
include('template/parziali/_panel_activiti.php');
include('template/parziali/_panel_chat.php');
//include('template/parziali/_panel_explore.php');
include('template/include/footer.php');
?>

