<?php
include('template/include/header.php');
include('template/include/siedenav.php');
?>
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
    <?php
    $breadcrumb = 'Dashboard Agenzia';
    include('template/include/topnav.php');
    ?>
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="container" id="kt_content_container">


            <div class="row g-xl-12 mb-5">


                <div class="col-xl-4">
                <div class="card mb-5 mb-xl-8">

                    <div class="card-body pt-15">

                        <div class="d-flex flex-wrap flex-sm-nowrap mb-6">
                            <div class="d-flex flex-center flex-shrink-0 bg-light rounded w-100px h-100px w-lg-100px h-lg-100px me-7 mb-4">
                                <img class="mw-50px mw-lg-75px" src="assets/media/svg/brand-logos/volicity-9.svg" alt="image">
                            </div>
                            <div class="flex-grow-1">
                                <!--begin::Head-->
                                <div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
                                    <!--begin::Details-->
                                    <div class="d-flex flex-column">
                                        <!--begin::Status-->
                                        <div class="d-flex align-items-center mb-1">
                                            <a href="#" class="text-gray-800 text-hover-primary fs-2 fw-bolder me-3">Nome Agenzia</a>
                                        </div>

                                        <div class="d-flex flex-wrap fw-bold fs-6 pe-2">
                                            <a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary me-5">
                                                <span class="svg-icon svg-icon-4 me-1">
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"></rect>
        <path d="M13,5 L15,5 L15,20 L13,20 L13,5 Z M5,5 L5,20 L3,20 C2.44771525,20 2,19.5522847 2,19 L2,6 C2,5.44771525 2.44771525,5 3,5 L5,5 Z M16,5 L18,5 L18,20 L16,20 L16,5 Z M20,5 L21,5 C21.5522847,5 22,5.44771525 22,6 L22,19 C22,19.5522847 21.5522847,20 21,20 L20,20 L20,5 Z" fill="#000000"></path>
        <polygon fill="#000000" opacity="0.3" points="9 5 9 20 7 20 7 5"></polygon>
    </g>
															</svg>
														</span>
                                                000000123010410414104</a>
                                        </div>



                                    </div>




                                    <div class="d-flex mb-4 d-none">
                                        <a href="#" class="btn btn-sm btn-bg-light btn-active-color-primary me-3" data-bs-toggle="modal" data-bs-target="#kt_modal_users_search">Add User</a>
                                        <a href="#" class="btn btn-sm btn-primary me-3" data-bs-toggle="modal" data-bs-target="#kt_modal_new_target">Add Target</a>
                                        <!--begin::Menu-->
                                        <div class="me-0">
                                            <button class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end" data-kt-menu-flip="top-end">
                                                <i class="bi bi-three-dots fs-3"></i>
                                            </button>
                                            <!--begin::Menu 3-->
                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px py-3" data-kt-menu="true">
                                                <!--begin::Heading-->
                                                <div class="menu-item px-3">
                                                    <div class="menu-content text-muted pb-2 px-3 fs-7 text-uppercase">Payments</div>
                                                </div>
                                                <!--end::Heading-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a href="#" class="menu-link px-3">Create Invoice</a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a href="#" class="menu-link flex-stack px-3">Create Payment
                                                        <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="Specify a target name for future usage and reference" aria-label="Specify a target name for future usage and reference"></i></a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3">
                                                    <a href="#" class="menu-link px-3">Generate Bill</a>
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start" data-kt-menu-flip="center, top">
                                                    <a href="#" class="menu-link px-3">
                                                        <span class="menu-title">Subscription</span>
                                                        <span class="menu-arrow"></span>
                                                    </a>
                                                    <!--begin::Menu sub-->
                                                    <div class="menu-sub menu-sub-dropdown w-175px py-4">
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3">Plans</a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3">Billing</a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <a href="#" class="menu-link px-3">Statements</a>
                                                        </div>
                                                        <!--end::Menu item-->
                                                        <!--begin::Menu separator-->
                                                        <div class="separator my-2"></div>
                                                        <!--end::Menu separator-->
                                                        <!--begin::Menu item-->
                                                        <div class="menu-item px-3">
                                                            <div class="menu-content px-3">
                                                                <!--begin::Switch-->
                                                                <label class="form-check form-switch form-check-custom form-check-solid">
                                                                    <!--begin::Input-->
                                                                    <input class="form-check-input w-30px h-20px" type="checkbox" value="1" checked="checked" name="notifications">
                                                                    <!--end::Input-->
                                                                    <!--end::Label-->
                                                                    <span class="form-check-label text-muted fs-6">Recuring</span>
                                                                    <!--end::Label-->
                                                                </label>
                                                                <!--end::Switch-->
                                                            </div>
                                                        </div>
                                                        <!--end::Menu item-->
                                                    </div>
                                                    <!--end::Menu sub-->
                                                </div>
                                                <!--end::Menu item-->
                                                <!--begin::Menu item-->
                                                <div class="menu-item px-3 my-1">
                                                    <a href="#" class="menu-link px-3">Settings</a>
                                                </div>
                                                <!--end::Menu item-->
                                            </div>
                                            <!--end::Menu 3-->
                                        </div>
                                        <!--end::Menu-->
                                    </div>



                                </div>



                            </div>
                        </div>


                        <div class="d-flex flex-stack py-1">
                            <div class="fw-bolder fs-6">Email:</div>
                            <div class="text-gray-600">
                                <a href="#" class="text-gray-600 text-hover-primary">nomeagenzia@mail.com</a>
                            </div>
                        </div>
                        <div class="d-flex flex-stack py-1">
                            <div class="fw-bolder fs-6">Telefono:</div>
                            <div class="text-gray-600">
                                <a href="#" class="text-gray-600 text-hover-primary">+39 0000000000000</a>
                            </div>
                        </div>
                        <div class="d-flex flex-stack py-1">
                            <div class="fw-bolder fs-6">Location:</div>
                            <div class="text-gray-600">Torino</div>
                        </div>
                        <div class="d-flex flex-stack py-1">
                            <div class="fw-bolder fs-6">Registrato:</div>
                            <div class="text-gray-600">10/01/2020</div>
                        </div>
                        <div class="d-flex flex-stack py-1">
                            <div class="fw-bolder fs-6">Ultimo accesso:</div>
                            <div class="text-gray-600">10/09/2021</div>
                        </div>


                        <div class="d-flex flex-stack py-3 mt-5">

                            <span data-bs-toggle="tooltip" data-bs-trigger="hover" title="" data-bs-original-title="Vai al profilo agenzia" class="w-100">
													<a href="#" class="btn btn-sm btn-light-primary w-100 fs-6" data-bs-toggle="modal"
                                                       data-bs-target="#kt_modal_update_customer">Vai al
                                                        profilo</a>
												</span>
                        </div>


                    </div>

                </div>

                </div>



            <div class="col-xl-4">
                <div class="card card-xl-stretch mb-xl-8">

                    <div class="card-header border-0 bg-primary py-5">
                        <h3 class="card-title fw-bolder text-white">Statistiche  Totali</h3>
                    </div>

                    <div class="card-body p-0">

                        <div class="statistiche-totali-chart card-rounded-bottom bg-primary" data-kt-color="primary" style="height: 200px"></div>


                        <div class="card-rounded bg-body mt-n10 position-relative card-px py-8">

                            <div class="row g-0 mb-7">

                                <div class="col mx-5">
                                    <div class="fs-6 text-gray-400">Ad Lorde</div>
                                    <div class="fs-2 fw-bolder text-gray-800">7.605</div>
                                </div>

                                <div class="col mx-5">
                                    <div class="fs-6 text-gray-400">Reject</div>
                                    <div class="fs-2 fw-bolder text-gray-800">1.706</div>
                                </div>

                            </div>

                            <div class="row g-0">

                                <div class="col mx-5">
                                    <div class="fs-6 text-gray-400">Ad Nette</div>
                                    <div class="fs-2 fw-bolder text-gray-800">5.899</div>
                                </div>

                                <div class="col mx-5">
                                    <div class="fs-6 text-gray-400">1° L</div>
                                    <div class="fs-2 fw-bolder text-gray-800">988</div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>
            <script>
                var initStatisticheTotali = function() {
                    var charts = document.querySelectorAll('.statistiche-totali-chart');

                    var color;
                    var strokeColor;
                    var height;
                    var labelColor = KTUtil.getCssVariableValue('--bs-gray-500');
                    var borderColor = KTUtil.getCssVariableValue('--bs-gray-200');
                    var options;
                    var chart;

                    [].slice.call(charts).map(function(element) {
                        height = parseInt(KTUtil.css(element, 'height'));

                        var options = {
                            series: [{
                                name: 'Ad Lorde',
                                data: [35, 65, 75, 55, 45, 60, 55]
                            }, {
                                name: 'Reject',
                                data: [40, 70, 80, 60, 50, 65, 60]
                            }],
                            chart: {
                                fontFamily: 'inherit',
                                type: 'bar',
                                height: height,
                                toolbar: {
                                    show: false
                                },
                                sparkline: {
                                    enabled: true
                                },
                            },
                            plotOptions: {
                                bar: {
                                    horizontal: false,
                                    columnWidth: ['30%'],
                                    endingShape: 'rounded'
                                },
                            },
                            legend: {
                                show: false
                            },
                            dataLabels: {
                                enabled: false
                            },
                            stroke: {
                                show: true,
                                width: 1,
                                colors: ['transparent']
                            },
                            xaxis: {
                                categories: ['Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug'],
                                axisBorder: {
                                    show: false,
                                },
                                axisTicks: {
                                    show: false
                                },
                                labels: {
                                    style: {
                                        colors: labelColor,
                                        fontSize: '12px'
                                    }
                                }
                            },
                            yaxis: {
                                min: 0,
                                max: 100,
                                labels: {
                                    style: {
                                        colors: labelColor,
                                        fontSize: '12px'
                                    }
                                }
                            },
                            fill: {
                                type: ['solid', 'solid'],
                                opacity: [0.25, 1]
                            },
                            states: {
                                normal: {
                                    filter: {
                                        type: 'none',
                                        value: 0
                                    }
                                },
                                hover: {
                                    filter: {
                                        type: 'none',
                                        value: 0
                                    }
                                },
                                active: {
                                    allowMultipleDataPointsSelection: false,
                                    filter: {
                                        type: 'none',
                                        value: 0
                                    }
                                }
                            },
                            tooltip: {
                                style: {
                                    fontSize: '12px'
                                },
                                y: {
                                    formatter: function (val) {
                                        return  val
                                    }
                                },
                                marker: {
                                    show: false
                                }
                            },
                            colors: ['#ffffff', '#ffffff'],
                            grid: {
                                borderColor: borderColor,
                                strokeDashArray: 4,
                                yaxis: {
                                    lines: {
                                        show: true
                                    }
                                },
                                padding: {
                                    left: 20,
                                    right: 20
                                }
                            }
                        };

                        var chart = new ApexCharts(element, options);
                        chart.render()
                    });
                }
                initStatisticheTotali();
            </script>


                <div class="col-xl-4">

                    <div class="card card-xl-stretch mb-xl-8">

                        <div class="card-header border-0 py-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">Statistiche mese</span>
                                <span class="text-muted fw-bold fs-7">2.500 Vendite</span>
                            </h3>
                        </div>

                        <div class="card-body p-0 d-flex flex-column">

                            <div class="card-p pt-5 bg-body flex-grow-1">

                                <div class="row g-0">

                                    <div class="col mr-8">

                                        <div class="fs-7 text-muted fw-bold">Ad Lorde</div>

                                        <div class="d-flex align-items-center">
                                            <div class="fs-4 fw-bolder">2.100</div>
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-up.svg-->
                                            <span class="svg-icon svg-icon-5 svg-icon-success ms-1">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M6.70710678,12.7071068 C6.31658249,13.0976311 5.68341751,13.0976311 5.29289322,12.7071068 C4.90236893,12.3165825 4.90236893,11.6834175 5.29289322,11.2928932 L11.2928932,5.29289322 C11.6714722,4.91431428 12.2810586,4.90106866 12.6757246,5.26284586 L18.6757246,10.7628459 C19.0828436,11.1360383 19.1103465,11.7686056 18.7371541,12.1757246 C18.3639617,12.5828436 17.7313944,12.6103465 17.3242754,12.2371541 L12.0300757,7.38413782 L6.70710678,12.7071068 Z" fill="#000000" fill-rule="nonzero" />
																	</g>
																</svg>
															</span>
                                        </div>
                                    </div>

                                    <div class="col">
                                        <div class="fs-7 text-muted fw-bold">Reject</div>

                                        <div class="fs-4 fw-bolder">233</div>
                                    </div>

                                </div>

                                <div class="row g-0 mt-8">

                                    <div class="col mr-8">

                                        <div class="fs-7 text-muted fw-bold">Ad Nette</div>

                                        <div class="fs-4 fw-bolder">1.290</div>

                                    </div>

                                    <div class="col">

                                        <div class="fs-7 text-muted fw-bold">Ad Media</div>

                                        <div class="d-flex align-items-center">
                                            <div class="fs-4 fw-bolder">500</div>
                                            <!--begin::Svg Icon | path: icons/duotone/Navigation/Arrow-down.svg-->
                                            <span class="svg-icon svg-icon-5 svg-icon-danger ms-1">
																<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																	<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																		<polygon points="0 0 24 0 24 24 0 24" />
																		<rect fill="#000000" opacity="0.5" x="11" y="5" width="2" height="14" rx="1" />
																		<path d="M6.70710678,18.7071068 C6.31658249,19.0976311 5.68341751,19.0976311 5.29289322,18.7071068 C4.90236893,18.3165825 4.90236893,17.6834175 5.29289322,17.2928932 L11.2928932,11.2928932 C11.6714722,10.9143143 12.2810586,10.9010687 12.6757246,11.2628459 L18.6757246,16.7628459 C19.0828436,17.1360383 19.1103465,17.7686056 18.7371541,18.1757246 C18.3639617,18.5828436 17.7313944,18.6103465 17.3242754,18.2371541 L12.0300757,13.3841378 L6.70710678,18.7071068 Z" fill="#000000" fill-rule="nonzero" transform="translate(12.000003, 14.999999) scale(1, -1) translate(-12.000003, -14.999999)" />
																	</g>
																</svg>
															</span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="statistiche-mese-chart card-rounded-bottom" data-kt-chart-color="primary" style="height: 150px"></div>
                        </div>

                    </div>

                </div>

                <script>
                    var initStatisticheMese = function() {
                        var charts = document.querySelectorAll('.statistiche-mese-chart');

                        [].slice.call(charts).map(function(element) {
                            var height = parseInt(KTUtil.css(element, 'height'));

                            if ( !element ) {
                                return;
                            }

                            var color = element.getAttribute('data-kt-chart-color');

                            var labelColor = KTUtil.getCssVariableValue('--bs-' + 'gray-800');
                            var strokeColor = KTUtil.getCssVariableValue('--bs-' + 'gray-300');
                            var baseColor = KTUtil.getCssVariableValue('--bs-' + color);
                            var lightColor = KTUtil.getCssVariableValue('--bs-light-' + color );

                            var options = {
                                series: [{
                                    name: 'Ad Lorde',
                                    data: [30, 25, 45, 30, 55, 55]
                                }],
                                chart: {
                                    fontFamily: 'inherit',
                                    type: 'area',
                                    height: height,
                                    toolbar: {
                                        show: false
                                    },
                                    zoom: {
                                        enabled: false
                                    },
                                    sparkline: {
                                        enabled: true
                                    }
                                },
                                plotOptions: {},
                                legend: {
                                    show: false
                                },
                                dataLabels: {
                                    enabled: false
                                },
                                fill: {
                                    type: 'solid',
                                    opacity: 1
                                },
                                stroke: {
                                    curve: 'smooth',
                                    show: true,
                                    width: 3,
                                    colors: [baseColor]
                                },
                                xaxis: {
                                    categories: ['Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug'],
                                    axisBorder: {
                                        show: false,
                                    },
                                    axisTicks: {
                                        show: false
                                    },
                                    labels: {
                                        show: false,
                                        style: {
                                            colors: labelColor,
                                            fontSize: '12px'
                                        }
                                    },
                                    crosshairs: {
                                        show: false,
                                        position: 'front',
                                        stroke: {
                                            color: strokeColor,
                                            width: 1,
                                            dashArray: 3
                                        }
                                    },
                                    tooltip: {
                                        enabled: true,
                                        formatter: undefined,
                                        offsetY: 0,
                                        style: {
                                            fontSize: '12px'
                                        }
                                    }
                                },
                                yaxis: {
                                    min: 0,
                                    max: 60,
                                    labels: {
                                        show: false,
                                        style: {
                                            colors: labelColor,
                                            fontSize: '12px'
                                        }
                                    }
                                },
                                states: {
                                    normal: {
                                        filter: {
                                            type: 'none',
                                            value: 0
                                        }
                                    },
                                    hover: {
                                        filter: {
                                            type: 'none',
                                            value: 0
                                        }
                                    },
                                    active: {
                                        allowMultipleDataPointsSelection: false,
                                        filter: {
                                            type: 'none',
                                            value: 0
                                        }
                                    }
                                },
                                tooltip: {
                                    style: {
                                        fontSize: '12px'
                                    },
                                    y: {
                                        formatter: function (val) {
                                            return val
                                        }
                                    }
                                },
                                colors: [lightColor],
                                markers: {
                                    colors: [lightColor],
                                    strokeColor: [baseColor],
                                    strokeWidth: 3
                                }
                            };

                            var chart = new ApexCharts(element, options);
                            chart.render();
                        });
                    }
                    initStatisticheMese();
                </script>

            </div>







            <div class="row g-6 g-xl-9 mb-5">
                <div class="col-xxl-8">
                    <div class="row">
                        <div class="col-xl-6 mb-5 mb-xl-10">
                            <div class="row">
                                <div class="col-xl-6">
                                    <?php
                                    include('template/moduli/wdg-tooks-square-blue.php');
                                    ?>
                                </div>
                                <div class="col-xl-6">
                                    <?php
                                    include('template/moduli/wdg-pe-square-yellow.php');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 mb-5 mb-xl-10">
                            <div class="row">
                                <div class="col-xl-6">
                                    <?php
                                    include('template/moduli/wdg-tooks-square-blue.php');
                                    ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <?php
                    include('template/moduli/wdg-badge-square-white.php');
                    ?>
                </div>
            </div>




            <div class="row g-6 g-xl-9 mb-5">
                <div class="col-xxl-8">
                    <div class="row">

                        <div class="col-xl-6 mb-5 mb-xl-10">


                        </div>


                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <?php
                            include('template/moduli/wdg-statistiche-square-blue.php');
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <?php
                            include('template/moduli/wdg-statistiche-square-red.php');
                            ?>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
    <?php
    include('template/include/footer-content.php');
    ?>
</div>
<?php
include('template/include/footer.php');
?>

