<?php
include('template/include/header.php');
include('template/include/siedenav.php');
?>
<div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
    <?php
    $breadcrumb = 'Dashboard';
    include('template/include/topnav.php');
    ?>
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="container" id="kt_content_container">
            <?php
            include('template/moduli/dialogatore/header-profilo.php');
            ?>
            <div class="row g-6 g-xl-9 mb-5">
                <div class="col-xxl-8">
                    <div class="row">
                        <div class="col-xl-6 mb-5 mb-xl-10">
                            <div class="row">
                                <div class="col-xl-6">
                                    <?php
                                    include('template/moduli/wdg-tooks-square-blue.php');
                                    ?>
                                </div>
                                <div class="col-xl-6">
                                    <?php
                                    include('template/moduli/wdg-pe-square-yellow.php');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 mb-5 mb-xl-10">
                            <?php
                            include('template/moduli/wdg-badge-square-white.php');
                            ?>
                        </div>
                        <div class="col-xl-6 mb-5">
                            <?php
                            include('template/moduli/wdg-statistiche-square-blue.php');
                            ?>
                        </div>
                        <div class="col-xl-6 mb-5">
                            <?php
                            include('template/moduli/wdg-statistiche-square-red.php');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <?php
                    include('template/moduli/wdg-completa-profilo.php');
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
    include('template/include/footer-content.php');
    ?>
</div>
<?php
include('template/include/footer.php');
?>

